#include "qxtwindowsystem.h"
/****************************************************************************
** Copyright (c) 2006 - 2011, the LibQxt project.
** See the Qxt AUTHORS file for a list of authors and copyright holders.
** All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are met:
**     * Redistributions of source code must retain the above copyright
**       notice, this list of conditions and the following disclaimer.
**     * Redistributions in binary form must reproduce the above copyright
**       notice, this list of conditions and the following disclaimer in the
**       documentation and/or other materials provided with the distribution.
**     * Neither the name of the LibQxt project nor the
**       names of its contributors may be used to endorse or promote products
**       derived from this software without specific prior written permission.
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
** WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
** DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
** (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
** LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
** ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
** SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
** <http://libqxt.org>  <foundation@libqxt.org>
*****************************************************************************/

#include <QLibrary>
#if QT_VERSION < QT_VERSION_CHECK(5,0,0)
#include <QX11Info>
#else
#include "QtX11Extras/qx11info_x11.h"
#endif
#include <X11/Xutil.h>

static QString windowClassName(WId window)
{
    QString className;

    XClassHint wmClass;
    wmClass.res_name = 0;
    wmClass.res_class = 0;

    if (XGetClassHint(QX11Info::display(), window, &wmClass) && wmClass.res_name) {
        className = QString::fromLocal8Bit(wmClass.res_name);
    }
    if (wmClass.res_name) {
        XFree(wmClass.res_name);
    }
    if (wmClass.res_class) {
        XFree(wmClass.res_class);
    }

    return className;
}

static QList<Window> widgetsToX11Windows(const QWidgetList& widgetList)
{
    QList<Window> windows;

    Q_FOREACH (const QWidget* widget, widgetList) {
        windows.append(widget->winId());
    }

    return windows;
}

static bool isTopLevelWindow(WId window)
{
    Display* display = QX11Info::display();

    static Atom atomWmState = 0;
    if (!atomWmState)
        atomWmState = XInternAtom(display, "WM_STATE", true);

    Atom type = None;
    int format;
    unsigned long nitems;
    unsigned long after;
    unsigned char* data = 0;
    int retVal = XGetWindowProperty(display, window, atomWmState, 0, 0, false, AnyPropertyType, &type, &format,
                                    &nitems, &after, &data);
    if (data) {
        XFree(data);
    }

    return (retVal == 0) && type;
}

static WindowList windowsRecursive(WId window)
{
    WindowList windows;

    if (isTopLevelWindow(window)) {
        windows.append(window);
    }

    Window root;
    Window parent;
    Window* children = 0;
    unsigned int numChildren;
    if (XQueryTree(QX11Info::display(), window, &root, &parent, &children, &numChildren) && children) {
        for (uint i = 0; i < numChildren; i++) {
            windows.append(windowsRecursive(children[i]));
        }
    }
    if (children) {
        XFree(children);
    }

    return windows;
}

WindowList QxtWindowSystem::windows()
{
    return windowsRecursive(QX11Info::appRootWindow());
}

WId QxtWindowSystem::activeWindow()
{
    Window window;
    int revertToReturn;
    XGetInputFocus(QX11Info::display(), &window, &revertToReturn);

    int tree;
    do {
        if (isTopLevelWindow(window)) {
            break;
        }

        Window root;
        Window parent;
        Window* children = 0;
        unsigned int numChildren;
        tree = XQueryTree(QX11Info::display(), window, &root, &parent, &children, &numChildren);
        window = parent;
        if (children) {
            XFree(children);
        }
    } while (tree && window);

    return window;
}

QString QxtWindowSystem::windowTitle(WId window)
{
    QString title;

    Display* display = QX11Info::display();
    Window rootWindow = QX11Info::appRootWindow();
    static Atom atomWmName = 0;
    static Atom atomNetWmName = 0;
    static Atom atomString = 0;
    static Atom atomUtf8String = 0;
    if (!atomWmName)
        atomWmName = XInternAtom(display, "WM_NAME", true);
    if (!atomNetWmName)
        atomNetWmName = XInternAtom(display, "_NET_WM_NAME", true);
    if (!atomString)
        atomString = XInternAtom(display, "STRING", true);
    if (!atomUtf8String)
        atomUtf8String = XInternAtom(display, "UTF8_STRING", true);

    Atom type;
    int format;
    unsigned long nitems;
    unsigned long after;
    unsigned char* data = 0;

    int retVal = XGetWindowProperty(display, window, atomNetWmName, 0, 1000, false, atomUtf8String,
                                    &type, &format, &nitems, &after, &data);

    if (retVal != 0 && data) {
        title = QString::fromUtf8(reinterpret_cast<char*>(data));
    } else {
        XTextProperty textProp;
        retVal = XGetTextProperty(display, window, &textProp, atomWmName);
        if (retVal != 0 && textProp.value) {
            char** textList = 0;
            int count;

            if (textProp.encoding == atomUtf8String) {
                title = QString::fromUtf8(reinterpret_cast<char*>(textProp.value));
            } else if (XmbTextPropertyToTextList(display, &textProp, &textList, &count) == 0 && textList && count > 0) {
                title = QString::fromLocal8Bit(textList[0]);
            } else if (textProp.encoding == atomString) {
                title = QString::fromLocal8Bit(reinterpret_cast<char*>(textProp.value));
            }

            if (textList) {
                XFreeStringList(textList);
            }
        }

        if (textProp.value) {
            XFree(textProp.value);
        }
    }

    if (data) {
        XFree(data);
    }

    //    if (useBlacklist && !title.isEmpty()) {
    //        if (window == rootWindow) {
    //            return QString();
    //        }

    //        QString className = windowClassName(window);
    //        if (m_classBlacklist.contains(className)) {
    //            return QString();
    //        }

    //        QList<Window> keepassxWindows = widgetsToX11Windows(QApplication::topLevelWidgets());
    //        if (keepassxWindows.contains(window)) {
    //            return QString();
    //        }
    //    }

    return title;
}

WId QxtWindowSystem::findWindow(const QString& title)
{
    Window result = 0;
    WindowList list = windows();
    foreach (const Window &wid, list) {
        if (windowTitle(wid) == title) {
            result = wid;
            break;
        }
    }

    return result;
}

WId QxtWindowSystem::windowAt(const QPoint& pos)
{
    Window result = 0;
    WindowList list = windows();
    for (int i = list.size() - 1; i >= 0; --i) {
        WId wid = list.at(i);
        if (windowGeometry(wid).contains(pos)) {
            result = wid;
            break;
        }
    }

    return result;
}

QRect QxtWindowSystem::windowGeometry(WId window)
{
    int x, y;
    uint width, height, border, depth;
    Window root, child;
    Display* display = QX11Info::display();
    XGetGeometry(display, window, &root, &x, &y, &width, &height, &border, &depth);
    XTranslateCoordinates(display, window, root, x, y, &x, &y, &child);

    static Atom atomNetFrameExtens = 0;
    if (!atomNetFrameExtens)
        atomNetFrameExtens = XInternAtom(QX11Info::display(), "_NET_FRAME_EXTENTS", True);

    QRect rect(x, y, width, height);
    Atom type = 0;
    int format = 0;
    uchar* data = 0;
    ulong count, after;
    if (XGetWindowProperty(display, window, atomNetFrameExtens, 0, 4, False, AnyPropertyType,
                           &type, &format, &count, &after, &data) == Success) {
        // _NET_FRAME_EXTENTS, left, right, top, bottom, CARDINAL[4]/32
        if (count == 4) {
            long* extents = reinterpret_cast<long*>(data);
            rect.adjust(-extents[0], -extents[2], extents[1], extents[3]);
        }
        if (data)
            XFree(data);
    }

    return rect;
}

typedef struct {
    Window  window;     /* screen saver window - may not exist */
    int     state;      /* ScreenSaverOff, ScreenSaverOn, ScreenSaverDisabled*/
    int     kind;       /* ScreenSaverBlanked, ...Internal, ...External */
    unsigned long    til_or_since;   /* time til or since screen saver */
    unsigned long    idle;      /* total time since last user input */
    unsigned long   eventMask; /* currently selected events for this client */
} XScreenSaverInfo;

typedef XScreenSaverInfo* (*XScreenSaverAllocInfo)();
typedef Status (*XScreenSaverQueryInfo)(Display* display, Drawable* drawable, XScreenSaverInfo* info);

static XScreenSaverAllocInfo _xScreenSaverAllocInfo = 0;
static XScreenSaverQueryInfo _xScreenSaverQueryInfo = 0;

uint QxtWindowSystem::idleTime()
{
    static bool xssResolved = false;
    if (!xssResolved) {
        QLibrary xssLib(QLatin1String("Xss"), 1);
        if (xssLib.load()) {
            _xScreenSaverAllocInfo = (XScreenSaverAllocInfo) xssLib.resolve("XScreenSaverAllocInfo");
            _xScreenSaverQueryInfo = (XScreenSaverQueryInfo) xssLib.resolve("XScreenSaverQueryInfo");
            xssResolved = true;
        }
    }

    uint idle = 0;
    if (xssResolved) {
        XScreenSaverInfo* info = _xScreenSaverAllocInfo();
        const int screen = QX11Info::appScreen();
        #if QT_VERSION < QT_VERSION_CHECK(5,0,0)
        Qt::HANDLE rootWindow = QX11Info::appRootWindow(screen);
        #else
        Qt::HANDLE rootWindow = (void*)QX11Info::appRootWindow(screen);
        #endif
        _xScreenSaverQueryInfo(QX11Info::display(), (Drawable*) rootWindow, info);
        idle = info->idle;
        if (info)
            XFree(info);
    }

    return idle;
}
